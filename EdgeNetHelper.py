# -*- coding: utf-8 -*-
"""
Created on Mon Sep  6 23:51:40 2021

@author: pena
"""

import argparse
import json
import requests
import yaml

def parseUrl(url):
    # not fully safe or foolproof:
    if ".mp4" in url:
        return ".mp4"
    else:
        return "HLS"

# Parse state from config file
with open('config.yml','r') as file:
    conf = yaml.safe_load(file)
    
serviceid = ""
apikey = ""

try:
    serviceid = conf["service_id"]
    apikey = conf["api_key"]
except:
    print("Config failure. See help for info.")
    
# Parse command line args
parser = argparse.ArgumentParser(description='EdgeNet Video Enablement API helper tool')
parser.add_argument('call_type', type=str, help="API call name: either 'add' or 'getall'")
parser.add_argument('video_name', type=str, nargs='?', help="URL of video to add")
parser.add_argument('debug', type=str, nargs='?', help="debug, set to 'true' for verbosity")
args = parser.parse_args()

# Main
if __name__ == "__main__":
    if args.debug:
        print("Starting with API key + Service ID:")
        print(apikey)
        print(serviceid)
    if args.call_type == "add" and args.video_name:
        # Add a video
        url = "https://developerapis.stg-alefedge.com/et/api/v1/stream-tech/content/add?partner_name="+serviceid
        headers = {
            "accept":"application/json",
            "api_key":apikey,
            "Content-Type":"application/json"
        }
        data = {
            "urlList": [
                {
                "url":args.video_name,  
                "content_access":"public",
                "publish_access":"public",
                "partner_cloud_url":args.video_name
                }
            ]
        }
        print("Sending content /add request for content URL "+args.video_name)
        resp = requests.post(url=url,headers=headers,json=data)
        if args.debug:
            print("SENDING POST REQUEST WITH HEADERS")
            print(headers)
            print("AND DATA")
            print(data)         
            print("RAW RESPONSE RECEIVED")
            print(resp.text)
        print(resp.json())
        print("Please check the status of your upload by the get-all command")
    else:
        # Get all
        # Add a video
        url = "https://developerapis.stg-alefedge.com/et/api/v1/stream-tech/content/get-all?partner_name="+serviceid
        headers = {
            "accept":"application/json",
            "api_key":apikey,
        }
        resp = requests.get(url=url,headers=headers)
        if args.debug:
            print("SENDING GET REQUEST WITH HEADERS")
            print(headers)
            print("RAW RESPONSE RECEIVED")
            print(resp.text)
            print("RESPONSE JSON")
            print(json.dumps(resp.json(), indent=3,sort_keys=True))   
        for respItem in resp.json():
            if respItem["content_upload_status"] == "success":
                contentId = respItem["content_id"]
                cloudUrl = respItem["partner_content_url"]
                fileName = respItem["partner_content_file_name"]
                edgeUrl = respItem["content_url"]
                if parseUrl(cloudUrl) == ".mp4":
                    print("***")
                    print("Item "+contentId + ", filename "+ fileName)
                    print("is a .mp4-format file, and can be played with")
                    print("native HTML, for example by the following embed link")
                    embedUrl = '<embed src='+edgeUrl+' autostart="false" height="600" width="800" /></embed>'
                    print(embedUrl)
                    print("or in your player by the content URL:")
                    print(edgeUrl)
                    print("***")
                else:
                    print("***")
                    print("Item "+contentId + ", filename "+ fileName)
                    print("is an HLS format media file, and can be played with")
                    print("an external player library, using the url")
                    print(edgeUrl)
                    print("***")
            else:
                contentId = respItem["content_id"]
                cloudUrl = respItem["partner_content_url"]
                fileName = respItem["partner_content_file_name"]
                contentStatus = respItem["content_upload_status"]
                contentStatusMsg = respItem["content_status_message"]
                print("***")
                print("Item "+contentId + ", filename "+ fileName)
                print("is unable to play with status:")
                print(contentStatus)
                print(contentStatusMsg)
                print("***")
                
        
        
        
        