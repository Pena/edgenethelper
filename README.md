Alef Private Edge Video Enablement API helper tool. Version 0.x, forks and merge requests welcomed.

Replace values in config.yml with your credentials.

## About
A Python-based command-line tool to act as an interface to the Alef Private Edge Video Enablement API (https://developer.alefedge.com/get-started/play-your-first-edge-video/5g-video-streaming-tutorial/)

## Usage
$ python EdgeNetHelper.py -h

usage: EdgeNetHelper.py [-h] call_type [video_name] [debug]

EdgeNet Video Enablement API helper tool

positional arguments:

  call_type   API call name: either 'add' or 'getall'

  video_name  URL of video to add

  debug       debug, set to 'true' for verbosity

optional arguments:
  -h, --help  show this help message and exit
